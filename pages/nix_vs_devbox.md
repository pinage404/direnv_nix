---
layout: full
class: "content-table expand-column-middle"
---

<!-- markdownlint-disable MD013 -->

<Emoji>

| Nix (lang) |                  🆚                   | DevBox |
| ---------: | :----------------------------------: | :----- |
|          ✅ |             env as code              | ✅      |
|          ✅ | freeze dependencies<br />_precisely_ | ✅      |

</Emoji>

---
layout: full
class: "content-table expand-column-middle"
---

<Emoji>

| Nix (lang) |                             🆚                              | DevBox |
| ---------: | :--------------------------------------------------------: | :----- |
|          ✅ |                       config as code                       | ❌      |
|          ✅ | advanced use cases<br />_e.g. packaging custom dependency_ | ❌      |

</Emoji>

---
layout: full
class: "content-table expand-column-middle"
---

<Emoji>

|   Nix (lang) |                       🆚                       | DevBox                          |
| -----------: | :-------------------------------------------: | :------------------------------ |
|       hard ❌ | target specific version<br />`nodejs@21.6.2`  | ✅ [easy](https://www.nixhub.io) |
| Nix : hard ❌ |              learn new language               | ✅ easy : JSON                   |
|            ❌ | similar to others tools<br />`npm`, `Cargo` … | ✅                               |

</Emoji>

<!--
JSON -> Nix lang

pas un Nième nouveau langage custom que personne n'utilise

declarative 🆚 imperative
-->
