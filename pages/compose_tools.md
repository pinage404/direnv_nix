---
layout: section
---

## 1 + 1 = <3

---

### DirEnv + Nix = <3

---
layout: full
style: "overflow: auto;"
---

### DirEnv + Nix

`.envrc`

```sh
use flake
```

`flake.nix`

```nix
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShell = pkgs.mkShell {
          packages = [ pkgs.nodejs ];
        };
      });
}
```

---

```sh
nix flake init --template github:nix-community/nix-direnv
```
