---
selectable: true
lineNumbers: true
addons:
  - slidev-component-zoom
defaults:
  layout: center
canvasWidth: 700
---

# Gérer son environnement

sans prise de tete

---
hideInToc: true
---

## Organisation

<Toc minDepth="2" maxDepth="2" />

---

## Introduction

* Disclaimer
* Tour de table
  * vos attentes

<!--
> je ne suis pas Charles Xavier / Professor X : je ne suis pas télépathe
>
> donc si vous ne dites pas que vous ne comprennez pas, je ne peux pas le savoir
>
> il n'y a aucune questions stupides
>
> si vous ne dites pas que vous ne savez pas, vous n'apprendrez pas
>
> n'hésitez pas à demander tant que vous n'avez pas compris
-->

---

### Objectifs

<v-clicks>

* Faire découvrir de __nouvelles possibilités__
* Montrer qu'un environnement peut se gérer __simplement__
* __Inciter__ à mettre en place

</v-clicks>

---
src: ./pages/direnv.md
---

---
src: ./pages/nix.md
---

---
src: ./pages/compose_tools.md
---

---
src: ./pages/devbox.md
---

---
src: ./pages/compose_tools_devbox.md
---

## Résumé

2 logiciels (DirEnv + DevBox) pour gérer son environnement sans prise de tete

---

## Démo

---

## Cloture

Tour de table des attentes

---
src: ./pages/questions.html
---
